﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class Calculator
    {
        List<double> results;

        public Calculator()
        {
            results = new List<double>();
        }

        public double Add(double a, double b)
        {
            return a + b;
        }

        public double Sub(double a, double b)
        {
            return a - b;
        }

        public double Div(double a, double b)
        {
            return a / b;
        }

        public double Mul(double a, double b)
        {
            return a * b;
        }

        public int AddResult(double value)
        {
            results.Add(value);
            return results.Count;
        }

        public double GetResult(int index)
        {
            return results[index - 1];
        }

        public int GetResultsCount()
        {
            return results.Count;
        }
    }
}
