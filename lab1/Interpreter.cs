﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace lab1
{
    class Interpreter
    {
        public void Start()
        {
            const string helpMessage = "Usage:\n" +
                "\twhen first symbol on line is '>' - enter operand (number)\n" +
                "\twhen first symbol on line is '@' - enter operation\n" +
                "\t\toperation is one of '+', '-', '/', '*' or\n" +
                "\t\t\t'#' followed with number of evaluation step\n" +
                "\t\t\t'q' to exit";
            Console.WriteLine(helpMessage);

            Calculator calculator = new Calculator();
            double value = GetOperand();
            bool newValue = true;

            while (true)
            {
                if (newValue)
                    Console.WriteLine("[#" + calculator.AddResult(value) + "] = " + value);
                newValue = true;
                string operation = GetOperation();
                switch (operation[0])
                {
                    case '+':
                        value = calculator.Add(value, GetOperand());
                        break;
                    case '-':
                        value = calculator.Sub(value, GetOperand());
                        break;
                    case '/':
                        value = calculator.Div(value, GetOperand());
                        break;
                    case '*':
                        value = calculator.Mul(value, GetOperand());
                        break;
                    case '#':
                        int index = int.Parse(operation.Substring(1));
                        if (index > calculator.GetResultsCount())
                        {
                            newValue = false;
                            Console.WriteLine("Result not found");
                            continue;
                        }
                        value = calculator.GetResult(index);
                        break;
                    case 'q':
                        return;
                }
            }
        }

        private double GetOperand()
        {
            const string errorMessage = "Incorrect operand.";
            Regex regex = new Regex(@"^-?\d+(?:\.\d+)?$");
            string input;
            while (true)
            {
                Console.Write("> ");
                input = Console.ReadLine().Replace(',', '.');
                if (regex.IsMatch(input))
                {
                    break;
                }
                else
                {
                    Console.WriteLine(errorMessage);
                }
            }
            return double.Parse(input, System.Globalization.CultureInfo.InvariantCulture);
        }

        private string GetOperation()
        {
            const string errorMessage = "Incorrect operation.";
            Regex regex = new Regex(@"^(?:[-+/*q]|#[1-9]\d*)$");
            string input;
            while (true)
            {
                Console.Write("@: ");
                input = Console.ReadLine();
                if (regex.IsMatch(input))
                {
                    break;
                }
                else
                {
                    Console.WriteLine(errorMessage);
                }
            }
            return input;
        }
    }
}
